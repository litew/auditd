## makerules.sh: audit-2.8.4-4.el7.x86_64
## version: 2.5-20190610-01

## First rule - delete all existing rules.
-D

## Increase the buffers to survive stress events.
## Make this bigger for busy systems.
## 8560 octets per event * 8192 event queue size = ~67 MB of RAM consumption.
-b 8192

## Rate limit (goes to failure mode if reached).
#-r 1000
-r 0

## Set failure mode.
# 0 - silent
# 1 - printk
# 2 - panic
-f 0

## Make the loginuid immutable. This prevents tampering with the auid.
--loginuid-immutable

## start mode
## The normal action is to report the line and issue with the
## rule and exit immediately with an error to get the admin's attention to
## fix the rules.
## Comment out rules below to choose this start mode.

## This rule will cause auditctl to continue loading rules when it runs
## across an unsupported field or a rule with a syntax error however it will
## report an error at exit.
#-c

## This rule will cause auditctl to continue loading rules when it runs
## across a an unsupported or a rule with a syntax error but exit with success
## reason code.
-i

## END OF start mode

## don't audit
## This is for don't audit rules. We put these early because audit
## is a first match wins system. Uncomment the rules you want.

## Cron jobs fill the logs with stuff we normally don't want.
-a never,user -F subj_type=crond_t
-a never,exit -F subj_type=crond_t
-a always,exclude -F subj_type=crond_t

## This rule suppresses the time-change event when chrony does time updates.
-a never,exit -F arch=b64 -S adjtimex,settimeofday,clock_settime -F auid=unset -F uid=chrony -F subj_type=chronyd_t
-a never,exit -F arch=b32 -S adjtimex,settimeofday,clock_settime,stime -F auid=unset -F uid=chrony -F subj_type=chronyd_t

## This rule suppresses the time-change event when ntp does time updates.
-a never,exit -F arch=b64 -S adjtimex,settimeofday,clock_settime -F auid=unset -F uid=ntp -F subj_type=ntpd_t
-a never,exit -F arch=b32 -S adjtimex,settimeofday,clock_settime,stime -F auid=unset -F uid=ntp -F subj_type=ntpd_t

## This rules suppresses annoying messages from VMWare tools.
-a never,exit -F arch=b64 -S all -F exe=/usr/bin/vmtoolsd
-a never,exit -F arch=b32 -S all -F exe=/usr/bin/vmtoolsd
#-a never,exit -F arch=b64 -S fork -F success=0 -F path=/usr/lib/vmware-tools -F subj_type=initrc_t -F exit=-2
#-a never,exit -F arch=b32 -S fork -F success=0 -F path=/usr/lib/vmware-tools -F subj_type=initrc_t -F exit=-2

## Ignore Splunk executables
-a never,exit -F arch=b64 -S all -F exe=/opt/splunkforwarder/bin/splunk
-a never,exit -F arch=b32 -S all -F exe=/opt/splunkforwarder/bin/splunk
-a never,exit -F arch=b64 -S all -F exe=/opt/splunkforwarder/bin/splunkd
-a never,exit -F arch=b32 -S all -F exe=/opt/splunkforwarder/bin/splunkd
-a never,exit -F arch=b64 -S all -F exe=/opt/splunkforwarder/bin/btool
-a never,exit -F arch=b32 -S all -F exe=/opt/splunkforwarder/bin/btool

## Ignore Zabbix executables
-a never,exit -F arch=b64 -S all -F exe=/usr/sbin/zabbix_agentd
-a never,exit -F arch=b32 -S all -F exe=/usr/sbin/zabbix_agentd
-a never,exit -F arch=b64 -S all -F exe=/usr/bin/zabbix_sender
-a never,exit -F arch=b32 -S all -F exe=/usr/bin/zabbix_sender


## Enumerate event types to be excluded from logging by auditd.
## Commented out event types are recorded by default.
#-a always,exclude -F msgtype=ACCT_LOCK
#-a always,exclude -F msgtype=ACCT_UNLOCK
#-a always,exclude -F msgtype=ADD_GROUP
#-a always,exclude -F msgtype=ADD_USER
#-a always,exclude -F msgtype=ANOM_ABEND
#-a always,exclude -F msgtype=ANOM_ACCESS_FS
#-a always,exclude -F msgtype=ANOM_ADD_ACCT
#-a always,exclude -F msgtype=ANOM_AMTU_FAIL
#-a always,exclude -F msgtype=ANOM_CRYPTO_FAIL
#-a always,exclude -F msgtype=ANOM_DEL_ACCT
#-a always,exclude -F msgtype=ANOM_EXEC
#-a always,exclude -F msgtype=ANOM_LINK
#-a always,exclude -F msgtype=ANOM_LOGIN_ACCT
#-a always,exclude -F msgtype=ANOM_LOGIN_FAILURES
#-a always,exclude -F msgtype=ANOM_LOGIN_LOCATION
#-a always,exclude -F msgtype=ANOM_LOGIN_SERVICE
#-a always,exclude -F msgtype=ANOM_LOGIN_SESSIONS
#-a always,exclude -F msgtype=ANOM_LOGIN_TIME
#-a always,exclude -F msgtype=ANOM_MAX_DAC
#-a always,exclude -F msgtype=ANOM_MAX_MAC
#-a always,exclude -F msgtype=ANOM_MK_EXEC
#-a always,exclude -F msgtype=ANOM_MOD_ACCT
#-a always,exclude -F msgtype=ANOM_PROMISCUOUS
#-a always,exclude -F msgtype=ANOM_RBAC_FAIL
#-a always,exclude -F msgtype=ANOM_RBAC_INTEGRITY_FAIL
#-a always,exclude -F msgtype=ANOM_ROOT_TRANS
## Ignore SELinux events
#-a always,exclude -F msgtype=AVC
#-a always,exclude -F msgtype=AVC_PATH
#-a always,exclude -F msgtype=BPRM_FCAPS
#-a always,exclude -F msgtype=CAPSET
#-a always,exclude -F msgtype=CHGRP_ID
#-a always,exclude -F msgtype=CHUSER_ID
#-a always,exclude -F msgtype=CONFIG_CHANGE
#-a always,exclude -F msgtype=CRED_ACQ
#-a always,exclude -F msgtype=CRED_DISP
#-a always,exclude -F msgtype=CRED_REFR
#-a always,exclude -F msgtype=CRYPTO_FAILURE_USER
#-a always,exclude -F msgtype=CRYPTO_IKE_SA
#-a always,exclude -F msgtype=CRYPTO_IPSEC_SA
#-a always,exclude -F msgtype=CRYPTO_KEY_USER
#-a always,exclude -F msgtype=CRYPTO_LOGIN
#-a always,exclude -F msgtype=CRYPTO_LOGOUT
#-a always,exclude -F msgtype=CRYPTO_PARAM_CHANGE_USER
#-a always,exclude -F msgtype=CRYPTO_REPLAY_USER
#-a always,exclude -F msgtype=CRYPTO_SESSION
#-a always,exclude -F msgtype=CRYPTO_TEST_USER
#-a always,exclude -F msgtype=CWD
#-a always,exclude -F msgtype=DAC_CHECK
#-a always,exclude -F msgtype=DAEMON_ABORT
#-a always,exclude -F msgtype=DAEMON_ACCEPT
#-a always,exclude -F msgtype=DAEMON_CLOSE
#-a always,exclude -F msgtype=DAEMON_CONFIG
#-a always,exclude -F msgtype=DAEMON_END
#-a always,exclude -F msgtype=DAEMON_ERR
#-a always,exclude -F msgtype=DAEMON_RESUME
#-a always,exclude -F msgtype=DAEMON_ROTATE
#-a always,exclude -F msgtype=DAEMON_START
#-a always,exclude -F msgtype=DEL_GROUP
#-a always,exclude -F msgtype=DEL_USER
#-a always,exclude -F msgtype=DEV_ALLOC
#-a always,exclude -F msgtype=DEV_DEALLOC
#-a always,exclude -F msgtype=EOE
#-a always,exclude -F msgtype=EXECVE
#-a always,exclude -F msgtype=FANOTIFY
#-a always,exclude -F msgtype=FD_PAIR
#-a always,exclude -F msgtype=FEATURE_CHANGE
#-a always,exclude -F msgtype=FS_RELABEL
#-a always,exclude -F msgtype=GRP_AUTH
#-a always,exclude -F msgtype=GRP_CHAUTHTOK
#-a always,exclude -F msgtype=GRP_MGMT
#-a always,exclude -F msgtype=INTEGRITY_DATA
#-a always,exclude -F msgtype=INTEGRITY_HASH
#-a always,exclude -F msgtype=INTEGRITY_METADATA
#-a always,exclude -F msgtype=INTEGRITY_PCR
#-a always,exclude -F msgtype=INTEGRITY_RULE
#-a always,exclude -F msgtype=INTEGRITY_STATUS
#-a always,exclude -F msgtype=IPC
#-a always,exclude -F msgtype=IPC_SET_PERM
#-a always,exclude -F msgtype=KERNEL
#-a always,exclude -F msgtype=KERNEL_OTHER
#-a always,exclude -F msgtype=KERN_MODULE
#-a always,exclude -F msgtype=LABEL_LEVEL_CHANGE
#-a always,exclude -F msgtype=LABEL_OVERRIDE
#-a always,exclude -F msgtype=LOGIN
#-a always,exclude -F msgtype=MAC_CHECK
#-a always,exclude -F msgtype=MAC_CIPSOV4_ADD
#-a always,exclude -F msgtype=MAC_CIPSOV4_DEL
#-a always,exclude -F msgtype=MAC_CONFIG_CHANGE
#-a always,exclude -F msgtype=MAC_IPSEC_ADDSA
#-a always,exclude -F msgtype=MAC_IPSEC_ADDSPD
#-a always,exclude -F msgtype=MAC_IPSEC_DELSA
#-a always,exclude -F msgtype=MAC_IPSEC_DELSPD
#-a always,exclude -F msgtype=MAC_IPSEC_EVENT
#-a always,exclude -F msgtype=MAC_MAP_ADD
#-a always,exclude -F msgtype=MAC_MAP_DEL
#-a always,exclude -F msgtype=MAC_POLICY_LOAD
#-a always,exclude -F msgtype=MAC_STATUS
#-a always,exclude -F msgtype=MAC_UNLBL_ALLOW
#-a always,exclude -F msgtype=MAC_UNLBL_STCADD
#-a always,exclude -F msgtype=MAC_UNLBL_STCDEL
#-a always,exclude -F msgtype=MMAP
#-a always,exclude -F msgtype=MQ_GETSETATTR
#-a always,exclude -F msgtype=MQ_NOTIFY
#-a always,exclude -F msgtype=MQ_OPEN
#-a always,exclude -F msgtype=MQ_SENDRECV
#-a always,exclude -F msgtype=NETFILTER_CFG
#-a always,exclude -F msgtype=NETFILTER_PKT
#-a always,exclude -F msgtype=OBJ_PID
#-a always,exclude -F msgtype=PATH
#-a always,exclude -F msgtype=PROCTITLE
#-a always,exclude -F msgtype=RESP_ACCT_LOCK
#-a always,exclude -F msgtype=RESP_ACCT_LOCK_TIMED
#-a always,exclude -F msgtype=RESP_ACCT_REMOTE
#-a always,exclude -F msgtype=RESP_ACCT_UNLOCK_TIMED
#-a always,exclude -F msgtype=RESP_ALERT
#-a always,exclude -F msgtype=RESP_ANOMALY
#-a always,exclude -F msgtype=RESP_EXEC
#-a always,exclude -F msgtype=RESP_HALT
#-a always,exclude -F msgtype=RESP_KILL_PROC
#-a always,exclude -F msgtype=RESP_SEBOOL
#-a always,exclude -F msgtype=RESP_SINGLE
#-a always,exclude -F msgtype=RESP_TERM_ACCESS
#-a always,exclude -F msgtype=RESP_TERM_LOCK
#-a always,exclude -F msgtype=ROLE_ASSIGN
#-a always,exclude -F msgtype=ROLE_MODIFY
#-a always,exclude -F msgtype=ROLE_REMOVE
#-a always,exclude -F msgtype=SECCOMP
## Ignore SELinux events
#-a always,exclude -F msgtype=SELINUX_ERR
#-a always,exclude -F msgtype=SERVICE_START
#-a always,exclude -F msgtype=SERVICE_STOP
#-a always,exclude -F msgtype=SOCKADDR
#-a always,exclude -F msgtype=SOCKETCALL
#-a always,exclude -F msgtype=SOFTWARE_UPDATE
#-a always,exclude -F msgtype=SYSCALL
#-a always,exclude -F msgtype=SYSTEM_BOOT
#-a always,exclude -F msgtype=SYSTEM_RUNLEVEL
#-a always,exclude -F msgtype=SYSTEM_SHUTDOWN
#-a always,exclude -F msgtype=TEST
#-a always,exclude -F msgtype=TRUSTED_APP
#-a always,exclude -F msgtype=TTY
#-a always,exclude -F msgtype=USER_ACCT
#-a always,exclude -F msgtype=USER_AUTH
## Ignore SELinux events
#-a always,exclude -F msgtype=USER_AVC
#-a always,exclude -F msgtype=USER_CHAUTHTOK
#-a always,exclude -F msgtype=USER_CMD
#-a always,exclude -F msgtype=USER_DEVICE
#-a always,exclude -F msgtype=USER_END
#-a always,exclude -F msgtype=USER_ERR
#-a always,exclude -F msgtype=USER_LABELED_EXPORT
#-a always,exclude -F msgtype=USER_LOGIN
#-a always,exclude -F msgtype=USER_LOGOUT
#-a always,exclude -F msgtype=USER_MAC_CONFIG_CHANGE
#-a always,exclude -F msgtype=USER_MAC_POLICY_LOAD
#-a always,exclude -F msgtype=USER_MGMT
#-a always,exclude -F msgtype=USER_ROLE_CHANGE
#-a always,exclude -F msgtype=USER_SELINUX_ERR
#-a always,exclude -F msgtype=USER_START
#-a always,exclude -F msgtype=USER_TTY
#-a always,exclude -F msgtype=USER_UNLABELED_EXPORT
#-a always,exclude -F msgtype=USYS_CONFIG
## Ignore KVM/QEMU (libvirt) events
#-a always,exclude -F msgtype=VIRT_CONTROL
#-a always,exclude -F msgtype=VIRT_CREATE
#-a always,exclude -F msgtype=VIRT_DESTROY
#-a always,exclude -F msgtype=VIRT_INTEGRITY_CHECK
#-a always,exclude -F msgtype=VIRT_MACHINE_ID
#-a always,exclude -F msgtype=VIRT_MIGRATE_IN
#-a always,exclude -F msgtype=VIRT_MIGRATE_OUT
#-a always,exclude -F msgtype=VIRT_RESOURCE
## END OF don't audit

## 32bit ABI
## If you are on a 64 bit platform, everything _should_ be running
## in 64 bit mode. This rule will detect any use of the 32 bit syscalls
## because this might be a sign of someone exploiting a hole in the 32
## bit API.
-a always,exit -F arch=b32 -S all -k 32bit-abi
## END OF 32bit ABI

## Unsuccessful accesses to
## objects and directories, including
## creation, open, close, modification, and deletion.

## unsuccessful creation
-a always,exit -F arch=b64 -S mkdir,creat,link,symlink,mknod,mknodat,linkat,symlinkat -F exit=-EACCES -k syscall-create-failed
-a always,exit -F arch=b32 -S creat,link,mknod,mkdir,symlink,mknodat,linkat,symlinkat -F exit=-EACCES -k syscall-create-failed
-a always,exit -F arch=b64 -S mkdir,link,symlink,mkdirat -F exit=-EPERM -k syscall-create-failed
-a always,exit -F arch=b32 -S link,mkdir,symlink,mkdirat -F exit=-EPERM -k syscall-create-failed
## END OF unsuccessful creation

## unsuccessful open
-a always,exit -F arch=b64 -S open,openat,open_by_handle_at -F exit=-EACCES -k syscall-open-failed
-a always,exit -F arch=b32 -S open,openat,open_by_handle_at -F exit=-EACCES -k syscall-open-failed
-a always,exit -F arch=b64 -S open,openat,open_by_handle_at -F exit=-EPERM -k syscall-open-failed
-a always,exit -F arch=b32 -S open,openat,open_by_handle_at -F exit=-EPERM -k syscall-open-failed
## END OF unsuccessful open

## unsuccessful close
-a always,exit -F arch=b64 -S close -F exit=-EIO -k syscall-close-failed
-a always,exit -F arch=b32 -S close -F exit=-EIO -k syscall-close-failed
## END OF unsuccessful close

## unsuccessful modifications
-a always,exit -F arch=b64 -S rename -S renameat -S truncate -S chmod -S setxattr -S lsetxattr -S removexattr -S lremovexattr -F exit=-EACCES -k syscall-mod-failed
-a always,exit -F arch=b32 -S rename -S renameat -S truncate -S chmod -S setxattr -S lsetxattr -S removexattr -S lremovexattr -F exit=-EACCES -k syscall-mod-failed
-a always,exit -F arch=b64 -S rename -S renameat -S truncate -S chmod -S setxattr -S lsetxattr -S removexattr -S lremovexattr -F exit=-EPERM -k syscall-mod-failed
-a always,exit -F arch=b32 -S rename -S renameat -S truncate -S chmod -S setxattr -S lsetxattr -S removexattr -S lremovexattr -F exit=-EPERM -k syscall-mod-failed
## ENF OF unsuccessful modifications

## unsuccessful deletion
-a always,exit -F arch=b64 -S rmdir,unlink,unlinkat -F exit=-EACCES -k syscall-delete-failed
-a always,exit -F arch=b32 -S unlink,rmdir,unlinkat -F exit=-EACCES -k syscall-delete-failed
-a always,exit -F arch=b64 -S rmdir,unlink,unlinkat -F exit=-EPERM -k syscall-delete-failed
-a always,exit -F arch=b32 -S unlink,rmdir,unlinkat -F exit=-EPERM -k syscall-delete-failed
## END OF unsuccessful deletion

## self-defense
-a always,exit -F dir=/var/log/audit/ -F perm=wa -k audit-log
-a always,exit -F dir=/etc/audit/ -F perm=wa -k audit-cfg
-a always,exit -F path=/etc/libaudit.conf -F perm=wa -k audit-cfg
-a always,exit -F dir=/etc/audisp/ -F perm=wa -k audit-cfg

-a always,exit -F exe=/usr/sbin/auditctl -F perm=x -k audit-util
-a always,exit -F exe=/usr/sbin/auditd -F perm=x -k audit-util
-a always,exit -F exe=/usr/sbin/aureport -F perm=x -k audit-util
-a always,exit -F exe=/usr/sbin/aulast -F perm=x -k audit-util
-a always,exit -F exe=/usr/sbin/aulastlogin -F perm=x -k audit-util
-a always,exit -F exe=/usr/sbin/auvirt -F perm=x -k audit-util
## END OF self-defense

## system config
-a always,exit -F dir=/etc/sysconfig/ -F perm=wa -k system-cfg
-a always,exit -F dir=/etc/default/ -F perm=wa -k system-cfg
## END OF system config

## rc.d & systemd
-a always,exit -F dir=/etc/rc.d/ -F perm=wa -k rc-d
-a always,exit -F dir=/usr/lib/systemd/system/ -F perm=wa -k systemd-lib-system
-a always,exit -F dir=/usr/lib/systemd/user/ -F perm=wa -k systemd-lib-user

-a always,exit -F dir=/etc/systemd/system/ -F perm=wa -k systemd-etc-system
-a always,exit -F dir=/etc/systemd/user/ -F perm=wa -k systemd-etc-user

-a always,exit -F exe=/usr/bin/systemctl -F perm=x -k systemd-util
## END OF rc.d & systemd

## time
-a always,exit -F path=/etc/localtime -F perm=wa -k time-cfg

-a always,exit -F arch=b64 -S adjtimex,settimeofday -k time-change
-a always,exit -F arch=b32 -S adjtimex,settimeofday,stime -k time-change
-a always,exit -F arch=b64 -S clock_settime -F a0=0x0 -k time-change
-a always,exit -F arch=b32 -S clock_settime -F a0=0x0 -k time-change
## END OF time

## cron scheduler
-a always,exit -F path=/etc/cron.allow -F perm=wa -k cron-cfg
-a always,exit -F path=/etc/cron.deny -F perm=wa -k cron-cfg
-a always,exit -F dir=/etc/cron.d/ -F perm=wa -k cron-cfg
-a always,exit -F dir=/etc/cron.daily/ -F perm=wa -k cron-cfg
-a always,exit -F dir=/etc/cron.hourly/ -F perm=wa -k cron-cfg
-a always,exit -F dir=/etc/cron.monthly/ -F perm=wa -k cron-cfg
-a always,exit -F dir=/etc/cron.weekly/ -F perm=wa -k cron-cfg
-a always,exit -F path=/etc/crontab -F perm=wa -k cron-cfg
-a always,exit -F dir=/var/spool/cron/ -F perm=wa -k cron-spool
## END OF cron scheduler

## accounts
-a always,exit -F path=/etc/group -F perm=wa -k accounts-cfg
-a always,exit -F path=/etc/passwd -F perm=wa -k accounts-cfg
-a always,exit -F path=/etc/gshadow -F perm=wa -k accounts-cfg
-a always,exit -F path=/etc/shadow -F perm=wa -k accounts-cfg
-a always,exit -F path=/etc/security/opasswd -F perm=wa -k accounts-cfg
-a always,exit -F path=/etc/nsswitch.conf -F perm=wa -k nsswitch-cfg

-a always,exit -F exe=/usr/bin/getent -F perm=wa -k accounts-util
## END OF accounts

## login defs & trails
-a always,exit -F path=/etc/login.defs -p wa -k logindefs
-a always,exit -F path=/etc/securetty -F perm=wa -k securetty
-a always,exit -F path=/var/log/tallylog -F perm=wa -k login-trail
-a always,exit -F path=/var/log/lastlog -F perm=wa -k login-trail
-a always,exit -F path=/var/log/btmp -F perm=wa -k login-trail
-a always,exit -F path=/var/log/wtmp -F perm=wa -k login-trail
-a always,exit -F path=/run/utmp -F perm=wa -k login-trail
## END OF login defs & trails

## network config
-a always,exit -F path=/etc/issue -F perm=wa -k network-cfg
-a always,exit -F path=/etc/issue.net -F perm=wa -k network-cfg
-a always,exit -F path=/etc/hosts -F perm=wa -k network-cfg
-a always,exit -F path=/etc/hostname -F perm=wa -k network-cfg
-a always,exit -F path=/etc/resolv.conf -F perm=wa -k network-cfg
-a always,exit -F dir=/etc/NetworkManager/ -F perm=wa -k network-cfg

-a always,exit -F exe=/usr/bin/nmcli -F perm=x -k network-util
-a always,exit -F exe=/usr/bin/nmtui -F perm=x -k network-util
-a always,exit -F exe=/usr/sbin/ifconfig -F perm=x -k network-util
-a always,exit -F exe=/usr/sbin/ifup -F perm=x -k network-util
-a always,exit -F exe=/usr/sbin/ifdown -F perm=x -k network-util
-a always,exit -F exe=/usr/sbin/ifcfg -F perm=x -k network-util
-a always,exit -F exe=/usr/sbin/ip -F perm=x -k network-util
-a always,exit -F exe=/usr/bin/firewall-cmd -F perm=x -k firewall-util
-a always,exit -F exe=/usr/bin/firewall-offline-cmd -F perm=x -k firewall-util
-a always,exit -F exe=/usr/sbin/xtables-multi -F perm=x -k firewall-util

-a always,exit -F arch=b64 -S sethostname,setdomainname -k system-name
-a always,exit -F arch=b32 -S sethostname,setdomainname -k system-name
## END OF network config

## SSH
-a always,exit -F path=/etc/ssh/ssh_config -F perm=wa -k ssh-cfg
-a always,exit -F path=/etc/ssh/sshd_config -F perm=wa -k sshd-cfg
## END OF SSH

## ld.so
## library search paths
-a always,exit -F path=/etc/ld.so.conf -F perm=wa -k ldso-cfg
-a always,exit -F dir=/etc/ld.so.conf.d/ -F perm=wa -k ldso-cfg
## END OF ld.so

## kernel
-a always,exit -F path=/etc/sysctl.conf -F perm=wa -k sysctl-cfg
-a always,exit -F dir=/etc/sysctl.d/ -F perm=wa -k sysctl-cfg
-a always,exit -F dir=/etc/modules-load.d/ -F perm=wa -k kmod-cfg
-a always,exit -F dir=/etc/modprobe.d/ -F perm=wa -k kmod-cfg
-a always,exit -F dir=/lib/modprobe.d/ -F perm=wa -k kmod-cfg

-a always,exit -F exe=/usr/bin/kmod -F perm=x -k kmod-util

-a always,exit -F arch=b32 -S init_module,finit_module -k kmod-load
-a always,exit -F arch=b64 -S init_module,finit_module -k kmod-load
-a always,exit -F arch=b32 -S delete_module -k kmod-unload
-a always,exit -F arch=b64 -S delete_module -k kmod-unload
## END OF kernel

## PAM configuration
-a always,exit -F dir=/etc/pam.d/ -F perm=wa -k pam-cfg
-a always,exit -F dir=/etc/security/ -F perm=wa -k pam-cfg

-a always,exit -F path=/usr/sbin/authconfig -F perm=x -k pam-util
-a always,exit -F path=/usr/sbin/authconfig-tui -F perm=x -k pam-util
## END OF PAM configuration

## package management
-a always,exit -F exe=/usr/bin/rpm -F perm=x -k pkg-mgmt-util
-a always,exit -F exe=/usr/bin/yum -F perm=x -k pkg-mgmt-util
## END OF package management

## SELinux
## Watch for changes to SELinux configuration
-a always,exit -F dir=/etc/selinux/ -F perm=wa -k selinux-cfg

-a always,exit -F exe=/usr/sbin/semanage -F perm=x -k selinux-util
-a always,exit -F exe=/usr/sbin/setsebool -F perm=x -k selinux-util
-a always,exit -F exe=/usr/sbin/setenforce -F perm=x -k selinux-util
## END OF SELinux

## powerstate
-a always,exit -F exe=/usr/sbin/shutdown -F perm=x -k powerstate-change
-a always,exit -F exe=/usr/sbin/poweroff -F perm=x -k powerstate-change
-a always,exit -F exe=/usr/sbin/reboot -F perm=x -k powerstate-change
-a always,exit -F exe=/usr/sbin/halt -F perm=x -k powerstate-change
## END OF powerstate

## mounts
-a always,exit -F arch=b64 -S mount -S umount2 -F auid!=-1 -k mounts-change
-a always,exit -F arch=b32 -S mount -S umount -S umount2 -F auid!=-1 -k mounts-change
## END OF mounts

## privilege escalation
-a always,exit -F path=/etc/sudoers -F perm=wa -k priv-esc-cfg
-a always,exit -F dir=/etc/sudoers.d/ -F perm=wa -k priv-esc-cfg
-a always,exit -F path=/etc/sudo.conf -F perm=wa -k priv-esc-cfg
-a always,exit -F path=/etc/sudo-ldap.conf -F perm=wa -k priv-esc-cfg

-a always,exit -F arch=b64 -F a0=0 -F exe=/usr/bin/su -F perm=x -k priv-esc-session
-a always,exit -F arch=b32 -F a0=0 -F exe=/usr/bin/su -F perm=x -k priv-esc-session
-a always,exit -F arch=b64 -F a0=0 -F exe=/usr/bin/sudo -F perm=x -k priv-esc-session
-a always,exit -F arch=b32 -F a0=0 -F exe=/usr/bin/sudo -F perm=x -k priv-esc-session

-a always,exit -F arch=b64 -S execve -C uid!=euid -F euid=0 -k priv-esc-session
-a always,exit -F arch=b32 -S execve -C uid!=euid -F euid=0 -k priv-esc-session

-a always,exit -F arch=b64 -S setuid -S setgid -S setreuid -S setregid -k priv-esc-syscall
-a always,exit -F arch=b32 -S setuid -S setgid -S setreuid -S setregid -k priv-esc-syscall
-a always,exit -F arch=b64 -S setuid -S setgid -S setreuid -S setregid -F exit=EPERM -k priv-esc-syscall-failed
-a always,exit -F arch=b32 -S setuid -S setgid -S setreuid -S setregid -F exit=EPERM -k priv-esc-syscall-failed
## END OF privilege escalation

## executable
-a always,exit -F arch=b64 -S execve -F euid=0 -k root-cmd-exec
-a always,exit -F arch=b32 -S execve -F euid=0 -k root-cmd-exec

-a always,exit -F arch=b64 -S execve -F euid>=1000 -k user-cmd-exec
-a always,exit -F arch=b32 -S execve -F euid>=1000 -k user-cmd-exec

-a always,exit -F arch=b64 -S execve -k cmd-exec
-a always,exit -F arch=b32 -S execve -k cmd-exec
## END OF executable

## file deletion by users
-a always,exit -F arch=b64 -S rmdir -S unlink -S unlinkat -S rename -S renameat -F auid=0 -k file-deletion-byroot
-a always,exit -F arch=b32 -S rmdir -S unlink -S unlinkat -S rename -S renameat -F auid=0 -k file-deletion-byroot

-a always,exit -F arch=b64 -S rmdir -S unlink -S unlinkat -S rename -S renameat -F auid>=1000 -F auid!=unset -k file-deletion-byuser
-a always,exit -F arch=b32 -S rmdir -S unlink -S unlinkat -S rename -S renameat -F auid>=1000 -F auid!=unset -k file-deletion-byuser
## END OF file deletion by users

## shell profiles
-a always,exit -F dir=/etc/skel -F perm=wa -k shell-profile
-a always,exit -F dir=/etc/profile.d/ -F perm=wa -k shell-profile
-a always,exit -F path=/etc/profile -F perm=wa -k shell-profile
-a always,exit -F path=/etc/shells -F perm=wa -k shell-profile
-a always,exit -F path=/etc/bashrc -F perm=wa -k shell-profile
-a always,exit -F path=/etc/csh.cshrc -F perm=wa -k shell-profile
-a always,exit -F path=/etc/csh.login -F perm=wa -k shell-profile
## END OF shell profiles

## injection
## These rules watch for code injection by the ptrace facility.
## This could indicate someone trying to do something bad or
## just debugging
-a always,exit -F arch=b64 -S ptrace -k tracing
-a always,exit -F arch=b32 -S ptrace -k tracing
-a always,exit -F arch=b64 -S ptrace -F a0=0x4 -k code-injection
-a always,exit -F arch=b32 -S ptrace -F a0=0x4 -k code-injection
-a always,exit -F arch=b64 -S ptrace -F a0=0x5 -k data-injection
-a always,exit -F arch=b32 -S ptrace -F a0=0x5 -k data-injection
-a always,exit -F arch=b64 -S ptrace -F a0=0x6 -k register-injection
-a always,exit -F arch=b32 -S ptrace -F a0=0x6 -k register-injection
## END OF injection

## network connections
## This is to check if the system is making or recieving connections
## externally

## ALL types of connect (AF_*)
#-a always,exit -F arch=b64 -S connect -k network-access-connect
#-a always,exit -F arch=b32 -S connect -k network-access-connect

## IPv4 connect (AF_INET)
-a always,exit -F arch=b64 -S connect -F a2=16 -k network-access-connect-ipv4
## IPv6 connect (AF_INET6)
-a always,exit -F arch=b64 -S connect -F a2=28 -k network-access-connect-ipv6

## ALL accept (AF_*)
-a always,exit -F arch=b64 -S accept -k network-access-accept
#-a always,exit -F arch=b32 -S accept -k network-access-accept

## ALL types of socket (AF_*)
#-a always,exit -F arch=b64 -S socket -k network-access-socket
#-a always,exit -F arch=b32 -S socket -k network-access-socket

## IPv4 socket (AF_INET)
-a always,exit -F arch=b64 -S socket -F a0=2 -k network-access-socket-ipv4
-a always,exit -F arch=b32 -S socket -F a0=2 -k network-access-socket-ipv4

## IPv6 socket (AF_INET6)
-a always,exit -F arch=b64 -S socket -F a0=10 -k network-access-socket-ipv6
-a always,exit -F arch=b32 -S socket -F a0=10 -k network-access-socket-ipv6
## END OF network connections

## enable flag
## 0 - disable auditd
## 1 - enable auditd and allow to change rules on the fly
## 2 - make the configuration immutable - reboot is required to change audit rules.
-e 1
